import psycopg2
import boto3
import json


def get_secret():
    secret_name = "wellfound/assessment/db"
    region_name = "us-east-1"
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name=region_name)
    get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    response = client.get_secret_value(
    SecretId="wellfound/assessment/db"
)

    secretDict = json.loads(response['SecretString'])
    return secretDict


def update_table(name, email):
    creds = get_secret()
    conn = psycopg2.connect(
        user=creds["username"],
        password=creds["password"],
        host=creds["host"],
        port=creds["port"]
    )
    cur = conn.cursor()

    command = (
        f"""
        INSERT INTO users (
            user_name,
            user_email
        )
        VALUES (
            {name!r},
            {email!r}
        )
        """
    )

    try:
        cur.execute(command)
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    
    if conn is not None:
        conn.close()


def create_table():
    creds = get_secret()
    conn = psycopg2.connect(
        user=creds["username"],
        password=creds["password"],
        host=creds["host"],
        port=creds["port"]
    )
    cur = conn.cursor()

    command = (
        """
        CREATE TABLE users (
            user_id SERIAL PRIMARY KEY,
            user_name VARCHAR(255) NOT NULL,
            user_email VARCHAR(255) NOT NULL
        )
        """
    )

    try:
        cur.execute(command)
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    
    if conn is not None:
        conn.close()